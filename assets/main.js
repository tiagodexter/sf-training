$(() => {
    
    const weekdays = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    const date = new Date();
    $('#date').html(`${weekdays[date.getDay()]}`);
    $('#time').html(` ${date.toLocaleString('en-US', { hour: 'numeric', hour12: true })}:${date.getMinutes()}:${date.getSeconds()}`);
    $("form").submit((e) => {
        e.preventDefault();
    });

    //Create a new string from a given string changing the position of first and last characters. The string length must be greater than or equal to 1
    $('#input-replace').keypress((e) => {
        if (e.keyCode == 13) {
            if ($('#input-replace').val().length > 1) {
                const result = changePosition($('#input-replace').val());
                $('#result-replace').html(result);
                $('#result-replace').css('color', 'black');
            } else {
                $('#result-replace').html('Please enter a string with more than one character');
                $('#result-replace').css('color', 'red');
            }
        }
    });
    $('#btn-replace').click((e) => {
        if ($('#input-replace').val().length > 1) {
            const result = changePosition($('#input-replace').val());
            $('#result-replace').html(result);
            $('#result-replace').css('color', 'black');
        } else {
            $('#result-replace').html('Please enter a string with more than one character');
            $('#result-replace').css('color', 'red');
        }
    });

    //---------------

    //Write a JavaScript function that reverse a number

    $('#input-reverse').keypress((e) => {
        if (e.keyCode == 13) {
            if ($('#input-reverse').val().length > 1) {
                const result = changePosition($('#input-reverse').val());
                $('#result-reverse').html(result);
                $('#result-reverse').css('color', 'black');
            } else {
                $('#result-reverse').html('Please enter a string with more than one character');
                $('#result-reverse').css('color', 'red');
            }
        }
    });
    $('#btn-reverse').click((e) => {
        if ($('#input-reverse').val().length > 1) {
            const result = reverseNumber($('#input-reverse').val());
            $('#result-reverse').html(result);
            $('#result-reverse').css('color', 'black');
        } else {
            $('#result-reverse').html('Please enter a number with more than one character');
            $('#result-reverse').css('color', 'red');
        }
    });

    //---------------

    // Write a JavaScript function that generates all combinations of a string

    $('#input-combination').keypress((e) => {
        if (e.keyCode == 13) {
            if ($('#input-combination').val().length > 1) {
                const result = changePosition($('#input-combination').val());
                $('#result-combination').html(result);
                $('#result-combination').css('color', 'black');
            } else {
                $('#result-combination').html('Please enter a string with more than one character');
                $('#result-combination').css('color', 'red');
            }
        }
    });
    $('#btn-combination').click((e) => {
        if ($('#input-combination').val().length > 1) {
            const result = allCombinations($('#input-combination').val());
            $('#result-combination').html("");
            result.forEach((e) => {
                $('#result-combination').append(`<li>${e}</li>`);
            });
            $('#result-combination').css('color', 'black');
        } else {
            $('#result-combination').html('Please enter a string with more than one character');
            $('#result-combination').css('color', 'red');
        }
    });

    //---------------
    //  Write a JavaScript function that accepts two arguments, a string and a letter and the function will count the number of occurrences of the specified letter within the string

    $('#input-two-arguments').keypress((e) => {
        if (e.keyCode == 13) {
            if ($('#input-two-arguments-1').val().length > 1) {
                const result = countLetters($('#input-two-arguments-1').val(),$('#input-two-arguments-2').val());
                $('#result-two-arguments').html(result);
                $('#result-two-arguments').css('color', 'black');
            } else {
                $('#result-two-arguments').html('Please enter a string with more than one character');
                $('#result-two-arguments').css('color', 'red');
            }
        }
    });
    $('#btn-two-arguments').click((e) => {
        if ($('#input-two-arguments-1').val().length > 1) {
            const result = countLetters($('#input-two-arguments-1').val(),$('#input-two-arguments-2').val());
            $('#result-two-arguments').html(result);
            $('#result-two-arguments').css('color', 'black');
        } else {
            $('#result-two-arguments').html('Please enter a string with more than one character');
            $('#result-two-arguments').css('color', 'red');
        }
    });

    //---------------
    // Write a JavaScript conditional statement to sort three numbers. Display an alert box to show the result

    $('#input-conditional-sort').keypress((e) => {
        if (e.keyCode == 13) {
            if ($('#input-conditional-sort').val().length > 1) {
                const ar = $('#input-conditional-sort').val().split(',');
                if (typeof ar == 'object') {
                    const result = conditionalSort(ar);
                    alert(result);
                    $('#result-conditional-sort').css('color', 'black');
                } else {
                    $('#result-conditional-sort').html('Please enter a string with comma separated');
                    $('#result-conditional-sort').css('color', 'red');
                }
            } else {
                $('#result-conditional-sort').html('Please enter a string with more than one character');
                $('#result-conditional-sort').css('color', 'red');
            }
        }
    });
    $('#btn-conditional-sort').click((e) => {
        if ($('#input-conditional-sort').val().length > 1) {
            const ar = $('#input-conditional-sort').val().split(',');
            if (typeof ar == 'object') {
                const result = conditionalSort(ar);
                alert(result);
                $('#result-conditional-sort').css('color', 'black');
            } else {
                $('#result-conditional-sort').html('Please enter a string with comma separated');
                $('#result-conditional-sort').css('color', 'red');
            }
        } else {
            $('#result-conditional-sort').html('Please enter a string with more than one character');
            $('#result-conditional-sort').css('color', 'red');
        }
    });

    //---------------
    //Write a JavaScript for loop that will iterate from 0 to 15. For each iteration, it will check if the current number is odd or even, and display a message to the screen Sample Output : "0 is even" "1 is odd" "2 is even"
    $('#btn-odd-even').click((e) => {
        const nloop = 15;
        for (let i = 0; i <= nloop; i++) {
            if (i % 2 == 0) {
                $('#result-odd-even').append(`<li>${i} is even</li>`);
            } else {
                $('#result-odd-even').append(`<li>${i} is odd</li>`);
            }
        }
    });

    //---------------
    //Write a JavaScript program to remove duplicate items from an array (ignore case sensitivity).

    $('#input-remove-duplicate').keypress((e) => {
        if (e.keyCode == 13) {
            if ($('#input-remove-duplicate').val().length > 1) {
                const ar = $('#input-remove-duplicate').val().split(',');
                if (typeof ar == 'object') {
                    const result = removeDuplicateFromArray(ar);
                    $('#result-remove-duplicate').html(result);
                    $('#result-remove-duplicate').css('color', 'black');
                } else {
                    $('#result-remove-duplicate').html('Please enter a string with comma separated');
                    $('#result-remove-duplicate').css('color', 'red');
                }
            } else {
                $('#result-remove-duplicate').html('Please enter a string with more than one character');
                $('#result-remove-duplicate').css('color', 'red');
            }
        }
    });

    $('#btn-remove-duplicate').click((e) => {
        if ($('#input-remove-duplicate').val().length > 1) {
            const ar = $('#input-remove-duplicate').val().split(',');
            if (typeof ar == 'object') {
                const result = removeDuplicateFromArray(ar);
                $('#result-remove-duplicate').html(result);
                $('#result-remove-duplicate').css('color', 'black');
            } else {
                $('#result-remove-duplicate').html('Please enter a string with comma separated');
                $('#result-remove-duplicate').css('color', 'red');
            }
        } else {
            $('#result-remove-duplicate').html('Please enter a string with more than one character');
            $('#result-remove-duplicate').css('color', 'red');
        }
    });

    //---------------


});

function changePosition(str) {
    const inputString = str;
    const firstLetter = inputString.substring(0, 1);
    const lastLetter = inputString.substring(inputString.length - 1);
    const newString = `${lastLetter}${inputString.substring(1, inputString.length - 1)}${firstLetter}`;
    return newString;
}

function reverseNumber(num) {
    return num.toString().split('').reverse().join('');
}

function allCombinations(str) {
    if (str.length < 2) {
        return str;
    } else {
        var permutations = [];
        for (var i = 0; i < str.length; i++) {
            var char = str[i];
            var permutationsOfRest = allCombinations(str.substring(0, i) + str.substring(i + 1));
            for (var j = 0; j < permutationsOfRest.length; j++) {
                if (permutations.indexOf(char + permutationsOfRest[j]) === -1) {
                    permutations.push(char + permutationsOfRest[j]);
                }
            }
        }
        return permutations;
    }
}

function countLetters(str,char) {
    let count = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === char) {
            count++;
        }
    }
    return count;
}

function conditionalSort(ar) {
    return ar.map((e) => {
        return e.trim();
    }).sort().reverse().join(',');
}

function removeDuplicateFromArray(ar) {
    return ar.filter((e, i) => {
        return ar.indexOf(e) === i;
    }).join(',');
}